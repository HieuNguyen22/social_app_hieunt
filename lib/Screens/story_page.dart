import 'package:chat_app_learn/Screens/info_page.dart';
import 'package:chat_app_learn/constants.dart';
import 'package:chat_app_learn/data/new_feed_json.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_icons/flutter_icons.dart';

class StoryPage extends StatefulWidget {
  const StoryPage({Key? key}) : super(key: key);

  @override
  State<StoryPage> createState() => _StoryPageState();
}

class _StoryPageState extends State<StoryPage> with TickerProviderStateMixin {
  late AnimationController controller;
  late var size;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..addListener(() {
        setState(() {});
      });
    controller.repeat(reverse: false);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      body: getBodyStory(),
    );
  }

  Widget getBodyStory() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
              child: Column(
            children: [
              Padding(
                  padding: EdgeInsets.only(left: 15, right: 15, bottom: 10),
                  child: Column(
                    children: [
                      // Progress bar to count down the story
                      LinearProgressIndicator(
                        value: controller.value,
                      ),
                      SizedBox(height: 12),
                      getUserInfo()
                    ],
                  )),
              getImageStory(),
              SizedBox(height: 15)
            ],
          )),
          getMessageBox()
        ],
      ),
    );
  }

  Widget getUserInfo() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          children: [
            Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(colors: bgStoryColors),
                ),
                child: Padding(
                    padding: const EdgeInsets.all(1.3),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) {
                          return InfoPage();
                        }));
                      },
                      child: Container(
                        height: 45,
                        width: 45,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(width: 1, color: bgWhite),
                            image: DecorationImage(
                                image: NetworkImage(
                                    newFeeds[0]['profile'].toString()),
                                fit: BoxFit.cover)),
                      ),
                    ))),
            SizedBox(width: 10),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                    text: TextSpan(
                        style: TextStyle(color: textBlack),
                        children: <TextSpan>[
                      TextSpan(
                          text: newFeeds[0]['username'].toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15)),
                      TextSpan(
                          text: '  ${newFeeds[0]['dateTime'].toString()}',
                          style: TextStyle(
                              color: textBlack.withOpacity(0.6), fontSize: 12)),
                    ])),
                Text(
                  '106 views',
                  style: TextStyle(
                      fontSize: 12, color: textBlack.withOpacity(0.8)),
                )
              ],
            ),
          ],
        ),
        Icon(
          FontAwesome.ellipsis_v,
          size: 15,
        )
      ],
    );
  }

  Widget getImageStory() {
    return Flexible(
      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(newFeeds[0]['imageUrl'].toString()),
                fit: BoxFit.cover)),
      ),
    );
  }

  Widget getMessageBox() {
    return Padding(
        padding: EdgeInsets.only(left: 10, right: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Flexible(
                child: TextFormField(
              decoration: InputDecoration(hintText: 'Send message'),
              textInputAction: TextInputAction.done,
              cursorColor: kPrimaryColor,
            )),
            Padding(
              padding: EdgeInsets.only(left: 10),
              child: Icon(Icons.send),
            )
          ],
        ));
  }
}
