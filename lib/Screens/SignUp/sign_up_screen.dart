// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:chat_app_learn/Screens/SignUp/components/sign_up_form.dart';
import 'package:chat_app_learn/constants.dart';
import 'package:flutter/material.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BodySignUp(),
    );
  }
}

class BodySignUp extends StatefulWidget {
  const BodySignUp({Key? key}) : super(key: key);

  @override
  State<BodySignUp> createState() => _BodySignUpState();
}

class _BodySignUpState extends State<BodySignUp> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 22.0),
          child: Text('Create an Account',
              style: TextStyle(
                  fontSize: 27,
                  color: kPrimaryColor,
                  fontWeight: FontWeight.bold)),
        ),
        Row(
          children: const[
            Spacer(),
            Expanded(child: SignUpForm(), flex: 8),
            Spacer()
          ],
        )
      ],
    );
  }
}
