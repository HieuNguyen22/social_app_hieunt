// ignore_for_file: prefer_const_constructors

import 'package:chat_app_learn/Screens/SignIn/sign_in_screen.dart';
import 'package:chat_app_learn/components/check_sign_in_sign_up.dart';
import 'package:chat_app_learn/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class SignUpForm extends StatelessWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
        child: Column(
      children: [
        TextFormField(
          keyboardType: TextInputType.name,
          textInputAction: TextInputAction.next,
          cursorColor: kPrimaryColor,
          onSaved: (email) => {},
          decoration: InputDecoration(
              hintText: "Username",
              prefixIcon: Padding(
                padding: const EdgeInsets.all(defaultPadding),
                child: Icon(Icons.person_outline),
              )),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: defaultPadding),
          child: TextFormField(
            keyboardType: TextInputType.emailAddress,
            textInputAction: TextInputAction.next,
            cursorColor: kPrimaryColor,
            decoration: InputDecoration(
                hintText: "Email address",
                prefixIcon: Padding(
                    padding: const EdgeInsets.all(defaultPadding),
                    child: Icon(Icons.email_outlined))),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: defaultPadding),
          child: TextFormField(
            textInputAction: TextInputAction.done,
            cursorColor: kPrimaryColor,
            decoration: InputDecoration(
                hintText: "Password",
                prefixIcon: Padding(
                  padding: const EdgeInsets.all(defaultPadding),
                  child: Icon(Icons.lock_outline),
                ),
                suffixIcon: Padding(
                  padding: const EdgeInsets.all(defaultPadding),
                  child: Icon(Icons.visibility_off),
                )
            ),
          ),
        ),
        const SizedBox(height: defaultPadding / 2),
        ElevatedButton(
          onPressed: () {},
          child: Text("Sign Up".toUpperCase()),
          style: ElevatedButton.styleFrom(
              maximumSize: const Size(double.infinity, 56),
              minimumSize: const Size(double.infinity, 56),
          ),
        ),
        const SizedBox(height: defaultPadding),
        CheckSignInSignUp(
            login: false,
            press: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return SignInScreen();
              }));
            })
      ],
    ));
  }
}
