// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:chat_app_learn/Screens/Main/Profile/components/get_user_fame.dart';
import 'package:chat_app_learn/Screens/Main/Profile/profile_page.dart';
import 'package:chat_app_learn/Screens/Main/wall_page.dart';
import 'package:chat_app_learn/constants.dart';
import 'package:chat_app_learn/data/new_feed_json.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class InfoPage extends StatefulWidget {
  const InfoPage({Key? key}) : super(key: key);

  @override
  State<InfoPage> createState() => _InfoPageState();
}

class _InfoPageState extends State<InfoPage> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: getBodyInfo(size),
    );
  }

  Widget getBodyInfo(size) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(newFeeds[0]['profile'].toString()),
              fit: BoxFit.cover)),
      child: Padding(
        padding: EdgeInsets.only(top: 35),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: EdgeInsets.only(left: 20),
                child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: 30,
                    ))),
            Container(
                color: bgDark.withOpacity(0.5),
                padding: EdgeInsets.only(left: 25, right: 20, top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      newFeeds[0]['username'].toString(),
                      style: TextStyle(
                          color: textWhite,
                          fontWeight: FontWeight.bold,
                          fontSize: 25),
                    ),
                    Text(
                      "Hair Dresser",
                      style: TextStyle(
                        color: textWhite,
                      ),
                    ),
                    SizedBox(height: 25),
                    getUserFame(size, Colors.white),
                    SizedBox(height: 25),
                    Row(
                      children: [
                        Flexible(
                            child: ElevatedButton(
                          onPressed: () {},
                          child: Text("Follow"),
                          style: ElevatedButton.styleFrom(
                            maximumSize: const Size(double.infinity, 40),
                            minimumSize: const Size(double.infinity, 40),
                          ),
                        )),
                        SizedBox(width: 10),
                        GestureDetector(
                            onTap: () {
                              Navigator.of(context)
                                  .push(MaterialPageRoute(builder: (context) {
                                return WallPage();
                              }));
                            },
                            child: Icon(
                              Icons.arrow_forward_ios_rounded,
                              color: Colors.white,
                              size: 30,
                            ))
                      ],
                    ),
                    SizedBox(height: 35)
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
