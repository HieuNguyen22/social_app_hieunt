// ignore_for_file: prefer_const_constructors

import 'package:chat_app_learn/Screens/Main/Follow/components/get_notifications.dart';
import 'package:chat_app_learn/Screens/Main/Follow/components/get_suggestions.dart';
import 'package:chat_app_learn/constants.dart';
import 'package:chat_app_learn/data/new_feed_json.dart';
import 'package:flutter/material.dart';

class FollowPage extends StatefulWidget {
  const FollowPage({Key? key}) : super(key: key);

  @override
  State<FollowPage> createState() => _FollowPageState();
}

class _FollowPageState extends State<FollowPage> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(body: getBodyFollow(size));
  }

  Widget getBodyFollow(size) {
    return ListView(children: [
      Padding(
          padding: const EdgeInsets.fromLTRB(25, 22, 15, 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getFollowRequest(),
              Divider(color: bgGrey),
              SizedBox(height: 15),
              GetNotifications(),
              SizedBox(height: 20),
              Text('Suggestions',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: textBlack.withOpacity(0.7),
                  )),
              SizedBox(height: 20),
              GetSuggestions()
            ],
          ))
    ]);
  }

  Widget getFollowRequest() {
    return Row(
      children: [
        Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 2, color: bgDark),
          ),
          child: Icon(Icons.person_add_outlined),
        ),
        SizedBox(width: 12),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Follow Requests',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 5),
            Text(
              "Approve or ignore request",
              style: TextStyle(fontSize: 12),
            )
          ],
        )
      ],
    );
  }
}
