import 'package:chat_app_learn/constants.dart';
import 'package:chat_app_learn/data/new_feed_json.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class GetNotifications extends StatefulWidget {
  const GetNotifications({Key? key}) : super(key: key);

  @override
  State<GetNotifications> createState() => _GetNotificationsState();
}

class _GetNotificationsState extends State<GetNotifications> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Today',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
              color: textBlack.withOpacity(0.7),
            )),
        SizedBox(height: 15),
        getTodayNotifications(size),
        SizedBox(height: 20),
        Text('Recent',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
              color: textBlack.withOpacity(0.7),
            )),
        SizedBox(height: 15),
        getRecentNotifications(size),
      ],
    );
  }

  Widget getTodayNotifications(size) {
    return Row(
      children: [
        Container(
            width: 55,
            child: Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 2, color: bgWhite),
                  image: DecorationImage(
                      image: NetworkImage(newFeeds[0]['profile'].toString()),
                      fit: BoxFit.cover)),
            )),
        SizedBox(width: 7),
        Container(
            width: size.width / 3 * 2,
            child: RichText(
              text: TextSpan(
                  style: TextStyle(color: textBlack),
                  children: <TextSpan>[
                    TextSpan(
                        text: '${newFeeds[0]['username']}',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: ' and'),
                    TextSpan(
                        text: ' 5 others',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        )),
                    TextSpan(text: ' started following you. 2d'),
                  ]),
            ))
      ],
    );
  }

  Widget getRecentNotifications(size) {
    return Column(
      children: [
        Row(
          children: [
            Container(
                width: 55,
                child: Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(width: 2, color: bgWhite),
                      image: DecorationImage(
                          image:
                              NetworkImage(newFeeds[1]['profile'].toString()),
                          fit: BoxFit.cover)),
                )),
            SizedBox(width: 7),
            Container(
                width: size.width / 3 * 2,
                child: RichText(
                  text: TextSpan(
                      style: TextStyle(color: textBlack),
                      children: <TextSpan>[
                        TextSpan(text: 'Follow '),
                        TextSpan(
                            text:
                                '${newFeeds[2]['username']}, ${newFeeds[3]['username']}',
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(
                          text:
                              ' and others you know to see their photos and videos. 5w',
                        ),
                      ]),
                ))
          ],
        ),
        SizedBox(height: 15),
        Row(
          children: [
            Container(
                width: 55,
                child: Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(width: 2, color: bgWhite),
                      image: DecorationImage(
                          image:
                              NetworkImage(newFeeds[3]['profile'].toString()),
                          fit: BoxFit.cover)),
                )),
            SizedBox(width: 7),
            Container(
                width: size.width / 3 * 2,
                child: RichText(
                  text: TextSpan(
                      style: TextStyle(color: textBlack),
                      children: <TextSpan>[
                        TextSpan(text: 'Follow '),
                        TextSpan(
                            text:
                                '${newFeeds[4]['username']}, ${newFeeds[1]['username']}',
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(
                          text:
                              ' and others you know to see their photos and videos. 5w',
                        ),
                      ]),
                ))
          ],
        ),
      ],
    );
  }
}
