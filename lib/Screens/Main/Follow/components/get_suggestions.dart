import 'package:chat_app_learn/constants.dart';
import 'package:chat_app_learn/data/new_feed_json.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class GetSuggestions extends StatefulWidget {
  const GetSuggestions({Key? key}) : super(key: key);

  @override
  State<GetSuggestions> createState() => _GetSuggestionsState();
}

class _GetSuggestionsState extends State<GetSuggestions> {
  @override
  Widget build(BuildContext context) {
    return Column(
        children: List.generate(newFeeds.length, (index) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 20, left: 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 55,
                  child: Stack(
                    children: [
                      Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(width: 2, color: bgWhite),
                            image: DecorationImage(
                                image: NetworkImage(
                                    newFeeds[index]['profile'].toString()),
                                fit: BoxFit.cover)),
                      ),
                      Positioned(
                        right: 10,
                        bottom: 0,
                        child: Container(
                            height: 10,
                            width: 10,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: Colors.green)),
                      )
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      newFeeds[index]['username'].toString(),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 2),
                    Text(
                      "@hyengye_2207",
                      style: TextStyle(color: textGrey, fontSize: 10),
                    ),
                    SizedBox(height: 2),
                    Text(
                      "Suggested for you",
                      style: TextStyle(color: textGrey, fontSize: 10),
                    )
                  ],
                )
              ],
            ),
            Row(
              children: [
                TextButton(
                  onPressed: () {},
                  child: Text(
                    "Follow",
                    style: TextStyle(color: textWhite),
                  ),
                  style: TextButton.styleFrom(backgroundColor: kPrimaryColor),
                ),
                SizedBox(width: 12),
                Icon(Icons.close_outlined, color: bgGrey, size: 17)
              ],
            )
          ],
        ),
      );
    }));
  }
}
