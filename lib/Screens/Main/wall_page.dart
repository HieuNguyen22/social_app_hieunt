import 'package:chat_app_learn/constants.dart';
import 'package:chat_app_learn/data/account_images_json.dart';
import 'package:chat_app_learn/data/new_feed_json.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class WallPage extends StatefulWidget {
  const WallPage({Key? key}) : super(key: key);

  @override
  State<WallPage> createState() => _WallPageState();
}

class _WallPageState extends State<WallPage> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(body: getBodyWall(size));
  }

  Widget getBodyWall(size) {
    return Padding(
      padding: EdgeInsets.fromLTRB(25, 20, 20, 10),
      child: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: EdgeInsets.only(left: 7, bottom: 17),
                  child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                        size: 22,
                      ))),
              getUserInfo(),
              SizedBox(height: 15),
              getUserFame(),
              SizedBox(height: 25),
              Text(
                'Posts',
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 25),
              getImages(size),
            ],
          )
        ],
      ),
    );
  }

  Widget getUserInfo() {
    return Padding(
        padding: EdgeInsets.only(left: 12),
        child: Row(
          children: [
            Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(colors: bgStoryColors),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(1.3),
                  child: Container(
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image:
                                NetworkImage(newFeeds[0]['profile'].toString()),
                            fit: BoxFit.cover)),
                  ),
                )),
            SizedBox(width: 15),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(newFeeds[0]['username'].toString(),
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
                Text(
                  'April 12',
                  style: TextStyle(
                      fontSize: 15, color: textBlack.withOpacity(0.8)),
                )
              ],
            ),
          ],
        ));
  }

  Widget getUserFame() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(width: 15),
        Flexible(
            child: ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(fixedSize: Size(100, 40)),
                child: Text(
                  'Follow',
                  style: TextStyle(color: textWhite),
                ))),
        SizedBox(width: 25),
        Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          Text('722',
              style: TextStyle(
                color: textBlack,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              )),
          Text(
            "Followers",
            style: TextStyle(color: textBlack, fontSize: 14, height: 1.5),
          ),
        ]),
        SizedBox(width: 25),
        Column(children: [
          Text('277',
              style: TextStyle(
                color: textBlack,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              )),
          Text(
            "Following",
            style: TextStyle(color: textBlack, fontSize: 14, height: 1.5),
          ),
        ])
      ],
    );
  }

  Widget getImages(size) {
    return Wrap(
        direction: Axis.horizontal,
        spacing: 10,
        runSpacing: 10,
        children: List.generate(images.length, (index) {
          return Container(
            height: (size.width - 60) / 2,
            width: (size.width - 60) / 2,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                image: DecorationImage(
                    image: NetworkImage(images[index]), fit: BoxFit.cover)),
          );
        }));
  }
}
