// ignore_for_file: prefer_const_constructors

import 'package:chat_app_learn/Screens/Main/Profile/components/get_complete_profile.dart';
import 'package:chat_app_learn/Screens/Main/Profile/components/get_user_fame.dart';
import 'package:chat_app_learn/Screens/Main/Profile/components/get_user_info.dart';
import 'package:chat_app_learn/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(body: getBodyProfile(size));
  }

  Widget getBodyProfile(size) {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // User info
              GetUserInfo(),
              SizedBox(height: 10),
              Divider(color: bgGrey),

              // Number of posts, followers, following
              getUserFame(size, Colors.black),
              Divider(
                color: bgGrey,
              ),
              SizedBox(height: 40),

              // Text
              Text(
                "Complete your profile",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 7),
              Text.rich(TextSpan(
                children: [
                  TextSpan(
                      text: '2 of 4',
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.green)),
                  TextSpan(
                      text: ' COMPLETED',
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: textGrey)),
                ],
              )),

              // Get progress of completing profile
              GetCompleteProfile()
            ],
          ),
        )
      ],
    );
  }
}
