import 'package:chat_app_learn/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class GetCompleteProfile extends StatefulWidget {
  const GetCompleteProfile({Key? key}) : super(key: key);

  @override
  State<GetCompleteProfile> createState() => _GetCompleteProfileState();
}

class _GetCompleteProfileState extends State<GetCompleteProfile> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.only(top: 20, left: 5, right: 5),
      child: Row(
          children: List.generate(4, (index) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(5.0, 2.0, 25.0, 2.0),
          child: Container(
            width: size.width / 2 + 30,
            height: size.width / 2 + 20,
            decoration: BoxDecoration(
                color: kPrimaryLightColor.withOpacity(0.5),
                border: Border.all(width: 1, color: bgGrey.withOpacity(0.4))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Icon(Icons.account_circle_outlined, size: 60),
                Text(
                  'Add profile photo',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 35, right: 35),
                  child: Text(
                    '15th available, always a first text look for',
                    textAlign: TextAlign.center,
                  ),
                ),
                ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                        minimumSize: Size(size.width / 6, 35)),
                    child: Text('Add photo'))
              ],
            ),
          ),
        );
      })),
    );
  }
}
