import 'package:flutter/material.dart';

Widget getUserFame(size, color) {
  return Container(
    width: (size.width - 20),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Column(children: [
          Text('100',
              style: TextStyle(
                  fontSize: 18, fontWeight: FontWeight.bold, color: color)),
          SizedBox(height: 8),
          Text(
            "Posts",
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                height: 1.5,
                color: color),
          ),
        ]),
        Column(children: [
          Text('722',
              style: TextStyle(
                  fontSize: 18, fontWeight: FontWeight.bold, color: color)),
          SizedBox(height: 8),
          Text(
            "Followers",
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                height: 1.5,
                color: color),
          ),
        ]),
        Column(children: [
          Text('277',
              style: TextStyle(
                  fontSize: 18, fontWeight: FontWeight.bold, color: color)),
          SizedBox(height: 8),
          Text(
            "Following",
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                height: 1.5,
                color: color),
          ),
        ])
      ],
    ),
  );
}
