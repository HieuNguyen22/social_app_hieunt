import 'package:chat_app_learn/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class GetUserInfo extends StatefulWidget {
  const GetUserInfo({Key? key}) : super(key: key);

  @override
  State<GetUserInfo> createState() => _GetUserInfoState();
}

class _GetUserInfoState extends State<GetUserInfo> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 100,
          width: 100,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 1, color: bgGrey),
              image: DecorationImage(
                  image: NetworkImage(profile), fit: BoxFit.cover)),
        ),
        SizedBox(height: 12),
        Text(
          instagramName,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: size.width * 0.3,
              child: Text(
                instagramBio,
                textAlign: TextAlign.left,
              ),
            ),
            TextButton(
                style: TextButton.styleFrom(backgroundColor: kPrimaryColor),
                onPressed: () {},
                child: const Text('Edit',
                    style: TextStyle(fontSize: 14, color: Colors.white)))
          ],
        ),
      ],
    );
  }
}
