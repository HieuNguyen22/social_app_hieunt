import 'package:chat_app_learn/Screens/Main/Follow/follow_page.dart';
import 'package:chat_app_learn/Screens/Main/Home/home_page.dart';
import 'package:chat_app_learn/Screens/Main/Profile/profile_page.dart';
import 'package:chat_app_learn/Screens/Main/Search/search_page.dart';
import 'package:chat_app_learn/constants.dart';
import 'package:chat_app_learn/data/bottom_navigation_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/svg.dart';

class Controller extends StatefulWidget {
  const Controller({Key? key}) : super(key: key);

  @override
  State<Controller> createState() => _ControllerState();
}

class _ControllerState extends State<Controller> {
  int indexPage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: getBody(),
      bottomNavigationBar: getBottomNavigationBar(),
    );
  }

  Widget getBody() {
    return IndexedStack(
      index: indexPage,
      children: [
        HomePage(),
        SearchPage(),
        FollowPage(),
        ProfilePage(),
      ],
    );
  }

  Widget getBottomNavigationBar() {
    return Container(
      height: 70,
      decoration: BoxDecoration(
        color: bgLightGrey,
        border: Border(top: BorderSide(width: 1, color: bgDark.withOpacity(0.3))),  
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: List.generate(icons.length, (index) {
            return IconButton(
              onPressed: () {
                setState(() {
                  indexPage = index;
                });
              }, 
              icon: SvgPicture.asset(
                // indexPage == index 
                // ? icons[index]['active']
                // : icons[index]['inactive'], 
                checkActive(icons, indexPage, index),
                color: kPrimaryColor,
                width: 25,
                height: 25
              ));
          }),
        ),
      ),
    );
  }

  String checkActive(List icons, int indexPage, int index) {
    if(indexPage == index) return icons[index]['active'];
    else return icons[index]['inactive'];
  }
}