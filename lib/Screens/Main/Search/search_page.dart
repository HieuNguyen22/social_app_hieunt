import 'package:chat_app_learn/constants.dart';
import 'package:chat_app_learn/data/new_feed_json.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(body: getBodySearch(size));
  }

  Widget getBodySearch(size) {
    return ListView(
      children: [
        SizedBox(height: 3),
        Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [getSearchBar(), getRecentSearch(), getResultSearch()],
          ),
        )
      ],
    );
  }

  Widget getSearchBar() {
    return TextFormField(
      textInputAction: TextInputAction.done,
      cursorColor: kPrimaryColor,
      decoration:
          InputDecoration(prefixIcon: Icon(Icons.search), hintText: "Search"),
    );
  }

  Widget getRecentSearch() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Text(
                'Recent',
                style: TextStyle(
                    color: textBlack.withOpacity(0.7),
                    fontWeight: FontWeight.bold),
              ),
            ),
            TextButton(
                onPressed: () {},
                child: Text(
                  "Clear",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ))
          ],
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.only(right: 5),
          child: Row(
            children: List.generate(newFeeds.length, (index) {
              return Padding(
                padding: EdgeInsets.fromLTRB(1.0, 2.0, 1.0, 2.0),
                child: Container(
                  width: 60,
                  child: Stack(
                    children: [
                      Container(
                        height: 55,
                        width: 55,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(width: 2, color: bgWhite),
                            image: DecorationImage(
                                image: NetworkImage(
                                    newFeeds[index]['profile'].toString()),
                                fit: BoxFit.cover)),
                      ),
                      Positioned(
                        right: 8,
                        bottom: 0,
                        child: Container(
                            height: 12,
                            width: 12,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: Colors.green)),
                      )
                    ],
                  ),
                ),
              );
            }),
          ),
        ),
        SizedBox(height: 25),
      ],
    );
  }

  Widget getResultSearch() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
            padding: EdgeInsets.only(left: 15),
            child: Text(
              "Result",
              style: TextStyle(
                  color: textBlack.withOpacity(0.7),
                  fontWeight: FontWeight.bold),
            )),
        SizedBox(height: 20),
        Column(
            children: List.generate(newFeeds.length, (index) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 20, left: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 55,
                      child: Stack(
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(width: 2, color: bgWhite),
                                image: DecorationImage(
                                    image: NetworkImage(
                                        newFeeds[index]['profile'].toString()),
                                    fit: BoxFit.cover)),
                          ),
                          Positioned(
                            right: 10,
                            bottom: 0,
                            child: Container(
                                height: 10,
                                width: 10,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.green)),
                          )
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          newFeeds[index]['username'].toString(),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 5),
                        Text(
                          "@hyengye_2207",
                          style: TextStyle(color: textGrey, fontSize: 10),
                        )
                      ],
                    )
                  ],
                ),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    "Follow",
                    style: TextStyle(color: textWhite),
                  ),
                  style: TextButton.styleFrom(backgroundColor: kPrimaryColor),
                )
              ],
            ),
          );
        }))
      ],
    );
  }
}
