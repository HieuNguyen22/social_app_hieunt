import 'package:chat_app_learn/Screens/Main/Home/components/get_news_feed.dart';
import 'package:chat_app_learn/Screens/Main/Home/components/get_stories.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(body: getBodyHome(size));
  }

  Widget getBodyHome(size) {
    return ListView(
      children: [GetStories(), Divider(), GetNewsFeed(), Column()],
    );
  }
}
