import 'package:chat_app_learn/constants.dart';
import 'package:chat_app_learn/data/new_feed_json.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class GetNewsFeed extends StatefulWidget {
  const GetNewsFeed({Key? key}) : super(key: key);

  @override
  State<GetNewsFeed> createState() => _GetNewsFeedState();
}

class _GetNewsFeedState extends State<GetNewsFeed> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      children: List.generate(newFeeds.length, (index) {
        return Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                getPostInfo(index),
                getPostImage(index, size),
                getPostReacts(index)
              ],
            ));
      }),
    );
  }

  Widget getPostInfo(index) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            children: [
              Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    gradient: LinearGradient(colors: bgStoryColors),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(1.3),
                    child: Container(
                      height: 35,
                      width: 35,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(width: 1, color: bgWhite),
                          image: DecorationImage(
                              image: NetworkImage(
                                  newFeeds[index]['profile'].toString()),
                              fit: BoxFit.cover)),
                    ),
                  )),
              SizedBox(width: 10),
              Text(
                newFeeds[index]['username'].toString(),
                style: TextStyle(fontWeight: FontWeight.bold),
              )
            ],
          ),
          Text(
            newFeeds[index]['dateTime'].toString(),
            style: TextStyle(fontSize: 12, color: textBlack),
          )
          // Icon(
          //   FontAwesome.ellipsis_v,
          //   size: 15,
          // )
        ],
      ),
    );
  }

  Widget getPostImage(index, size) {
    return Container(
      // Load image
      height: size.height / 3,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(newFeeds[index]['imageUrl'].toString()),
              fit: BoxFit.cover)),
    );
  }

  Widget getPostReacts(index) {
    return Padding(
        padding: EdgeInsets.only(top: 10, left: 15, right: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              // Load number of likes
              '${newFeeds[index]['likes']} likes',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 5),
            Text.rich(// Load caption
                TextSpan(
              children: [
                TextSpan(
                    text: newFeeds[index]['username'].toString(),
                    style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(
                    text: newFeeds[index]['caption'].toString(),
                    style: TextStyle(height: 1.5))
              ],
            )),
            SizedBox(height: 8),
            Text(
              newFeeds[index]['comments'].toString(),
              style: TextStyle(color: textGrey),
            )
          ],
        ));
  }
}
