import 'package:chat_app_learn/Screens/story_page.dart';
import 'package:chat_app_learn/constants.dart';
import 'package:chat_app_learn/data/story_json.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class GetStories extends StatefulWidget {
  const GetStories({Key? key}) : super(key: key);

  @override
  State<GetStories> createState() => _GetStoriesState();
}

class _GetStoriesState extends State<GetStories> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.only(top: 20, left: 5, right: 5),
        child: Row(
            children: List.generate(stories.length, (index) {
          return Padding(
              padding: const EdgeInsets.fromLTRB(1.0, 2.0, 1.0, 2.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return StoryPage();
                  }));
                },
                child: Container(
                  width: 70,
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          checkIsStories(index),
                          checkIsAdd(index)
                        ],
                      ),
                    ],
                  ),
                ),
              ));
        })));
  }

  Widget checkIsStories(index) {
    return (stories[index]['isStory'] == true) // Kiem tra xem co la Story hay k
        ? Container(
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(colors: bgStoryColors)),
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Container(
                height: 55,
                width: 55,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(width: 2, color: bgWhite),
                    image: DecorationImage(
                        image:
                            NetworkImage(stories[index]['imageUrl'].toString()),
                        fit: BoxFit.cover)),
              ),
            ),
          )
        : Padding(
            padding: const EdgeInsets.all(2.0),
            child: Container(
              height: 55,
              width: 55,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(width: 1, color: bgGrey),
                image: DecorationImage(
                    image: NetworkImage(stories[index]['imageUrl'].toString()),
                    fit: BoxFit.cover),
              ),
            ),
          );
  }

  Widget checkIsAdd(index) {
    return (stories[index]['isAdd'] == true) // Kiem tra co phai la Them story k
        ? Positioned(
            right: 5,
            bottom: 0,
            child: Container(
              height: 20,
              width: 20,
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: kPrimaryColor),
              child: Center(
                child: Icon(
                  Icons.add,
                  color: bgWhite,
                  size: 20,
                ),
              ),
            ),
          )
        : Container();
  }
}
