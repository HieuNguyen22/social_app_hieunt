// ignore_for_file: prefer_const_constructors

import 'package:chat_app_learn/Screens/SignIn/components/login_form.dart';
import 'package:chat_app_learn/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class SignInScreen extends StatelessWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: Text('Chat App')),
      body: BodySignIn(),
    );
  }
}

class BodySignIn extends StatefulWidget {
  const BodySignIn({Key? key}) : super(key: key);

  @override
  State<BodySignIn> createState() => _BodySignInState();
}

class _BodySignInState extends State<BodySignIn> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 22.0),
          child: Text(
            'Welcome Back!',
            style: TextStyle(
              fontSize: 27, 
              color: kPrimaryColor,
              fontWeight: FontWeight.bold),
          )
        ),
        Row(
          children: const [
            Spacer(),
            Expanded(
              child: LoginForm(),
              flex: 8,
            ),
            Spacer()
          ],
        )
      ],
    );
  }
}
