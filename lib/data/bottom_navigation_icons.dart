
const icons = [
  {
    "inactive": "assets/images/home.svg",
    "active": "assets/images/home_active.svg",
  },
  {
    "inactive": "assets/images/search.svg",
    "active": "assets/images/search_active.svg",
  },
  {
    "inactive": "assets/images/follow.svg",
    "active": "assets/images/follow_active.svg",
  },
  {
    "inactive": "assets/images/account.svg",
    "active": "assets/images/account_active.svg",
  },
];