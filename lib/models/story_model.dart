class Story {
  int id;
  String imgUrl;
  String username;
  bool isStory;
  bool isAdd;

  Story(
      {required this.id,
      required this.imgUrl,
      required this.username,
      required this.isStory,
      required this.isAdd});

  get getId => this.id;

  set setId(final id) => this.id = id;

  get getImgUrl => this.imgUrl;

  set setImgUrl(imgUrl) => this.imgUrl = imgUrl;

  get getUsername => this.username;

  set setUsername(username) => this.username = username;

  get getIsStory => this.isStory;

  set setIsStory(isStory) => this.isStory = isStory;

  get getIsAdd => this.isAdd;

  set setIsAdd(isAdd) => this.isAdd = isAdd;
}
