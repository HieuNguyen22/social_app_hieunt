import 'package:flutter/material.dart';

// --------------- Color ------------------
const kPrimaryColor = Colors.blue;
const kPrimaryLightColor = Color.fromARGB(255, 224, 236, 245);

const Color textWhite = Color(0xffffffff);
const Color textBlack = Color(0xff000000);
const Color textGrey = Color(0xffC8C8C8);

const Color bgWhite = Color(0xffffffff);
const Color bgDark = Color(0xff000000);
const Color bgGrey = Color(0xffC8C8C8);
const Color bgLightGrey = Color(0xffFAFAFA);

const bgStoryColors = [
  const Color(0xffA844A1),
  const Color(0xffAB429A),
  const Color(0xffB43C88),
  const Color(0xffC33269),
  const Color(0xffD7243F),
  const Color(0xffF9A326),
  const Color(0xffF9DD26),
];

const Color success = Color(0xff44c93a);
const Color danger = Color(0xffff4657);
const Color info = Color(0xff5bc0de);
const Color warning = Color(0xfffeba06);

// --------------- Padding ------------------
const double defaultPadding = 16.0;

// --------------- String ------------------
const String profile = "https://images.unsplash.com/photo-1529665253569-6d01c0eaf7b6?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cHJvZmlsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60";
const String username = "hyengye";

const String instagramName = "Hieu Nguyen";
const String instagramBio = "Life is riding a bike. Moving is balance.";